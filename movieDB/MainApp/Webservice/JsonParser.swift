//
//  JsonParser.swift
//  movieDB
//
//  Created by Miguel Garcia Topete on 8/2/16.
//  Copyright © 2016 MXNLabs. All rights reserved.
//

import UIKit

protocol JsonParserDelegate{
    
    func didParsedWithObjects(parsedObjects:[MOBMovie])
    
    func didFinishedParsingWithError(error:ErrorType)
}

class JsonParser: NSObject {
    
    var delegate:JsonParserDelegate?
    var parsedObjects = [MOBMovie]()
    func StartParsingWithData(data:NSData) {
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
            
            
            
            if let movies = json["results"] as? [[String: AnyObject]] {
                for movie in movies {
                    
                    let aMovie : MOBMovie = MOBMovie()
        
                    guard let id = movie["id"] as? Int,
                        let poster = movie["poster_path"] as? String,
                        let releaseDate = movie["release_date"] as? String,
                        let overview = movie["overview"] as? String,
                        let title = movie["title"] as? String else {
                            return;
                    }
                    aMovie.id = id
                    aMovie.poster_path = poster
                    aMovie.title = title
                    aMovie.release_date = releaseDate
                    aMovie.overview = overview
                    self.parsedObjects.append(aMovie)
                }
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    self.delegate?.didParsedWithObjects(self.parsedObjects)
                    
                })
                
            }
        } catch {
            print("error serializing JSON: \(error)")
            self.delegate?.didFinishedParsingWithError(error)
        }
    }
}
