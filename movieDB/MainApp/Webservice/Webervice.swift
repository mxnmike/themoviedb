//
//  Webervice.swift
//  movieDB
//
//  Created by Miguel Garcia Topete on 8/2/16.
//  Copyright © 2016 MXNLabs. All rights reserved.
//

import UIKit
protocol WebServiceDelegate {
    
    func didFinishWithData(data:NSData)
    func didFinishWithErrors(error:NSError)
}

class Webervice: NSObject {
    var delegate:WebServiceDelegate?
    
    func startDownloadingWithURL(url:NSURL) {
        
        let request = NSURLRequest(URL: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            
            if error==nil {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.delegate?.didFinishWithData(data!)
                })
                
                
            } else {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.delegate?.didFinishWithErrors(error!)
                })
                
            }
        })
        task.resume()
    }
    

}
