//
//  MainVCC.swift
//  movieDB
//
//  Created by Miguel Garcia Topete on 8/3/16.
//  Copyright © 2016 MXNLabs. All rights reserved.
//

import UIKit
let kBASE_URL = "http://api.themoviedb.org/3"
let kBASE_IMAGE_URL = "http://image.tmdb.org/t/p/w92"
let kNOWSHOWING_URL = "/discover/movie?api_key=432081ab18d1123e9f516f1b2c87679e&release_date.gte=2016-07-01&page="
let movieCellIdentifier = "movieCell"
class MainVC: UIViewController,WebServiceDelegate,JsonParserDelegate, UITableViewDelegate, UITableViewDataSource{



    

    //MARK:- - IBOutlets and Variables
    @IBOutlet weak var tableView: UITableView!
    var  moviesArray = [MOBMovie]()
    let dateFormatter = NSDateFormatter()
    let calendar = NSCalendar.currentCalendar()
    var refreshControl: UIRefreshControl!
    var page : Int = 1
    var urlString : String?
    var wsManager : Webervice?
    var isRefreshing : Bool?
    //MARK: - - ViewCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wsManager = Webervice()
        wsManager!.delegate = self
        urlString = "\(kBASE_URL+kNOWSHOWING_URL)"
        let url = NSURL(string:urlString! + String(page))
        wsManager!.startDownloadingWithURL(url!)
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.lightGrayColor()
        refreshControl.tintColor = UIColor.whiteColor()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(MainVC.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        tableView.addSubview(refreshControl)
        
        dateFormatter.dateFormat = "YYYY-MM-dd"
        tableView.registerNib(UINib.init(nibName:"MovieCell", bundle: nil), forCellReuseIdentifier: movieCellIdentifier)
//        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return moviesArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(movieCellIdentifier, forIndexPath: indexPath) as! MovieCell
        
        // Configure the cell...
        let tmpMovie = moviesArray[indexPath.row]
        
        let date = dateFormatter.dateFromString(tmpMovie.release_date!)
        
        let components = calendar.components([.Year], fromDate: date!)
        
        let year =  components.year
        cell.title.text = "\(tmpMovie.title!) (\(year))"
        cell.overview.text = tmpMovie.overview
        cell.posterImage.alpha = 0.0
        if tmpMovie.poster_image != nil {
            
            cell.posterImage.image = tmpMovie.poster_image
            cell.posterImage.alpha = 1.0
            
        } else {
            
            cell.posterImage.image = UIImage(named: "")
            let imageStringURL = kBASE_IMAGE_URL + tmpMovie.poster_path!
            let url:NSURL = NSURL(string:imageStringURL)!
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)){
                
                if let data = NSData(contentsOfURL: url) {
                    
                    tmpMovie.poster_image = UIImage(data: data)
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        cell.posterImage.image = UIImage(data: data)
                        UIView.animateWithDuration(0.25, animations: { 
                            cell.posterImage.alpha = 1.0
                        })
                    })
                } else {
                    
                    cell.posterImage.image = UIImage(named:"no-thumb")
                    
                }
            }
            
        }
        if (moviesArray.count - indexPath.row) <= 5 {
            
            page += 1;
            let url = NSURL(string:urlString! + String(page))
            wsManager?.startDownloadingWithURL(url!)
            
            
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- - Custom Methods
    func refresh(sender:AnyObject) {
        isRefreshing = true
        page = 1
        let url = NSURL(string:urlString! + String(page))
        wsManager?.startDownloadingWithURL(url!)


    }

    
    
    //MARK:- - WebHandlerDelegate Methods
    
    func didFinishWithData(data: NSData) {
        let jParser = JsonParser()
        jParser.delegate = self
        jParser.StartParsingWithData(data)
        
    }
    func didFinishWithErrors(error: NSError) {
        print("\(error.description)")
    }
    
    //MARK:- - JsonParserDelegate
    
    func didParsedWithObjects(parsedObjects: [MOBMovie]) {
        
        if page == 1 {
            if isRefreshing == true {
                self.refreshControl.endRefreshing()
            }
            moviesArray.removeAll()
        }
        moviesArray.appendContentsOf(parsedObjects)
        tableView.reloadData()
    }
    func didFinishedParsingWithError(error: ErrorType) {
        print("Error:\(error)")
    }
    
    

    
    
}
