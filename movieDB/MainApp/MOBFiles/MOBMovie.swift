//
//  MOBMovie.swift
//  movieDB
//
//  Created by Miguel Garcia Topete on 8/2/16.
//  Copyright © 2016 MXNLabs. All rights reserved.
//

import UIKit

class MOBMovie: NSObject {
    
    var id : Int?
    var poster_path : String?
    var poster_image : UIImage?
    var title : String?
    var release_date : String?
    var overview : String?
    
}

